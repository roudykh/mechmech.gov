<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="description" content=""/> 
	<meta name="keywords" content=""/>
    <link rel="stylesheet" type="text/css" href="./Common/styles/common.css"/>
    <link rel="stylesheet" type="text/css" href="./Common/styles/layout.css"/>
    <link rel="stylesheet" type="text/css" href="./Common/themes/grey-burgundy/grey-burgundy.css"/>
    <!-- Header Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/Header/html/styles/header.css"/>
    <!-- Menu Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/Menu/html/styles/menu.css"/>
    <script type="text/javascript" src="./Components/HTML/Menu/html/functions/functions.js"></script>
    <!-- Newsticker Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/NewsTicker/html/styles/newsticker.css"/>
    <!-- Breadcrumb Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/Breadcrumb/html/styles/breadcrumb.css"/>
    <!-- Footer Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/Footer/html/styles/footer.css"/>
    <!-- Copyrights Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/Copyright/html/styles/copyright.css"/>
    <!-- Main News -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/MainNews/html/styles/main-news.css"/>
    <script type="text/javascript" src="./Common/functions/jquery-1.4.min.js"></script>
    <script type="text/javascript" src="./Common/functions/functions.js"></script>
    <!-- Listing Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/ListingComponent/html/styles/listing-component.css"/>
    <!-- Bullets Listing Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/BulletListing/html/styles/bullet-listing.css"/>
    <!-- Bullets Listing Option 3 Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/BulletListing-Option3/html/styles/bullet-listing-opt3.css"/>
    <!-- One Item Big Option 1 Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/ListingOneItem-Option3/html/styles/listing-oneitem-big.css"/>
    <!-- Listing Option 3 Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/ListingComponent-Option3/html/styles/listing-component-opt3.css"/>
    <script type="text/javascript" src="./Components/HTML/ListingComponent-Option3/html/functions/functions.js"></script>
     <!-- Horizontal Listing Option 2 Styles -->
    <link rel="stylesheet" type="text/css" href="./Components/HTML/HorizontalListing-Option2/html/styles/horizontal-listing-opt2.css"/>
    <script type="text/javascript" src="./Components/HTML/HorizontalListing-Option2/html/functions/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="./Components/HTML/HorizontalListing-Option2/html/functions/functions.js"></script>
	<link rel="shortcut icon" href="./Common/images/favicon.ico"/>
	<title>بلدية مشمش</title>
	
	<style>
	* {box-sizing: border-box;}
	.mySlides {display: none;}
	img {vertical-align: middle;}

	/* Slideshow container */
	.slideshow-container {
	  max-width: 100%;
	  position: relative;
	  margin: auto;
	}

	/* Fading animation */
	.fade {
	  -webkit-animation-name: fade;
	  -webkit-animation-duration: 1.5s;
	  animation-name: fade;
	  animation-duration: 1.5s;
	}

	@-webkit-keyframes fade {
	  from {opacity: .4} 
	  to {opacity: 1}
	}

	@keyframes fade {
	  from {opacity: .4} 
	  to {opacity: 1}
	}
</style>
</head>
<body>
<!-- End Top Header -->
<!-- Header Container -->
<div class="" id="header">
    <div class="logosHeader">
    	<div class="mainLogoContainer"><img src="./Common/images/minstry-logo.gif" /></div>
        <div class="clearBoth"></div>
  	</div>
</div>
<!-- End Header Container -->
<!-- Menu Container -->
<div class="menuCont" id="menu">
	<div class="boxContainer">
	<div class="menuBtnsCont" style="width:100%">
        <ul class="menuBtnsList">
            <li><a href="#" class="firstMenuBtn selected">الصفحة الرئيسية</a></li>
			<li><a href="javascript:;" onmouseout="MM_showHideLayers('sub1','','hide');document.getElementById('menuBtn1').className='';" 
					onmouseover="MM_showHideLayers('sub1','','show');document.getElementById('menuBtn1').className='highlightMenuBtn';" 
					id="menuBtn1">المجلس البلدي</a><div class="relativePos highZindex"><div class="absoluteSubMenu" 
					id="sub1" onmouseout="MM_showHideLayers('sub1','','hide');document.getElementById('menuBtn1').className='';" 
					onmouseover="MM_showHideLayers('sub1','','show');document.getElementById('menuBtn1').className='highlightMenuBtn';">
					<div class="subMenuContainer">
                <div class="subMenuSection"><ul>
                <li><a href="./subpages/committee.html" class="mainTitle">المجلس</a></li>
                <li><a href="./subpages/presidentWord.html" class="mainTitle">كلمة الرئيس</a></li>
                </ul></div>
                <div class="clearBoth"></div>
            </div></div></div></li>
            <li><a href="./subpages/history.html">نبذة عنا</a></li>
            <li><a href="./subpages/achievements.html">الانجازات</a></li>
            <li><a href="./subpages/papersAndRequests.html">طلبات ومعاملات</a></li>
            <li><a href="javascript:;" onmouseout="MM_showHideLayers('sub3','','hide');document.getElementById('menuBtn3').className='';" 
					onmouseover="MM_showHideLayers('sub3','','show');document.getElementById('menuBtn3').className='highlightMenuBtn';" 
					id="menuBtn3">مؤسسات وجمعيات</a><div class="relativePos highZindex"><div class="absoluteSubMenu" 
					id="sub3" onmouseout="MM_showHideLayers('sub3','','hide');document.getElementById('menuBtn3').className='';" 
					onmouseover="MM_showHideLayers('sub3','','show');document.getElementById('menuBtn3').className='highlightMenuBtn';">
					<div class="subMenuContainer">
                <div class="subMenuSection"><ul>
                <li><a href="./subpages/school.html" class="mainTitle">المدرسة</a></li>
                <li><a href="./subpages/clinic.html" class="mainTitle">المستوصف الخيري</a></li>
                <li><a href="./subpages/church.html" class="mainTitle">الكنائس</a></li>
                </ul></div>
                <div class="subMenuSectionLast"><ul>
                <li><a href="./subpages/chabibe.html" class="mainTitle">الشبيبة والطلائع</a></li>
                <li><a href="./subpages/club.html" class="mainTitle">النادي الرياضي</a></li>
                <li><a href="./subpages/shop.html" class="mainTitle">محلات تجارية</a></li>
                </ul></div>
                <div class="clearBoth"></div>
            </div></div></div></li>
            <li><a href="./subpages/gallery.html">الصور</a></li>
            <li><a href="./subpages/announcements.php">الاعلانات</a></li>
            <li><a href="./subpages/contact.html">الإتصال بنا</a></li>
        </ul>
        </div>
    </div>
</div>
<!-- End Menu Container -->
<!-- Center Container -->
<div class="">
	<div class="centerContBackground" style="width:100%">
        <div class="">
        
			<div class="slideshow-container">
				<div class="mySlides fade">
				  <img src="./Common/images/slider1.jpg" style="width:100%">
				</div>

				<div class="mySlides fade">
				  <img src="./Common/images/slider2.jpg" style="width:100%">
				</div>

				<div class="mySlides fade">
				  <img src="./Common/images/slider3.jpg" style="width:100%">
				</div>
			</div>
			<div style="text-align:center">
			  <span class="dot"></span> 
			  <span class="dot"></span> 
			  <span class="dot"></span> 
			</div>
		</div>
		<!--<br>
		<div class="clearBoth"></div>
        <div class="Col12Sect" style="margin-left:50px">
            <div id="horizontalListing" class="containersSpace">
                <div class="Col12Sect">
                    <div id="listingOption2">
                    <div class="titlesContainer">
                        <div><h2><a href="./subpages/committee.html">
                        المجلس البلدي</a></h2></div>
                    </div>
                    <div class="listingNodes">
                        <ul>
                            <li class="borderBottomNone">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><p>يهدف المشروع إلى تأهيل ومساعدة 300 مزارع من يهدف المشروع إلى تأهيل ومساعدة 300 مزارع من يهدف المشروع إلى تأهيوع إلى تأهيل ومساعدة 300 مزارع من يهدف المشروع إلى تأهيوع إلى تأهيل ومساعدة 300 مزارع من يهدف المشروع إلى تأهيوع إلى تأهيل ومساعدة 300 مزارع من يهدف المشروع إلى تأهيوع إلى تأهيل ومساعدة 300 مزارع من يهدف المشروع إلى تأهيوع إلى تأهيل ومساعدة 300 مزارع منيهدف المشروع إلى تأهيل ومساعدة 300 مزارع منيهدف المشروع إلى تأهيل ومساعدة 300 مزارع من. <a href="./subpages/committee.html" class="moreLink">للمزيد</a></p></td>
                                </tr>
                            </table>
                            </li>
                            <li class="borderBottomNone">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="1" class="listImageContainerPad"><a href="./subpages/presidentWord.html" title="كلمة الرئيس"><img src="./Common/images/140-105.gif" alt="كلمة الرئيس" title="كلمة الرئيس" class="listingImageBorder"/></a></td>
                                    <td><h3><a href="./subpages/presidentWord.html">كلمة الرئيس</a></h3>
                                    <p>يهدف المشروع إلى تأهيل ومساعدة 300 مزارع من. <a href="./subpages/presidentWord.html" class="moreLink">للمزيد</a></p></td>
                                </tr>
                            </table>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
                
                <div class="clearBoth"></div><br>
            </div>
            <div id="HorizontalListingOpt2">
            <div class="titlesContainer">
                    <div><h2><a href="javascript:;">مؤسسات وجمعيات</a></h2></div>
                </div>
                <div class="horizontalListingCont">
				
				<table style="border-spacing: 50px 0;">
				  <tr>
					<td><div><a href="./subpages/school.html" title="عنوان يكتب هنا"><img src="./Common/images/school.jpg" alt="المدرسة" title="المدرسة"/></a></div>
                    <div class="summaryNewsContainer horizontalListCont">
                        <div class="listingDate">المدرسة</div>
                    </div><br></td>&emsp;
					<td><div><a href="./subpages/church.html" title="الكنائس"><img src="./Common/images/church.jpg" alt="الكنائس" title="الكنائس"/></a></div>
                    <div class="summaryNewsContainer horizontalListCont">
                        <div class="listingDate">الكنائس</div>
                    </div><br></td>
					<td><div><a href="./subpages/clinic.html" title="المستوصف الخيري"><img src="./Common/images/clinic.jpg" alt="المستوصف الخيري" title="المستوصف الخيري"/></a></div>
                    <div class="summaryNewsContainer horizontalListCont">
                        <div class="listingDate">المستوصف الخيري</div>
                    </div><br></td>
				  </tr>
				  <tr>
					<td><div><a href="./subpages/club.html" title="النادي الرياضي"><img src="./Common/images/club.jpg" alt="النادي الرياضي" title="النادي الرياضي"/></a></div>
                    <div class="summaryNewsContainer horizontalListCont">
                        <div class="listingDate">النادي الرياضي</div>
                    </div></td>
					<td><div><a href="./subpages/chabibe.html" title="عنوان يكتب هنا"><img src="./Common/images/chabibe.jpg" alt="الشبيبة والطلائع" title="الشبيبة والطلائع"/></a></div>
                    <div class="summaryNewsContainer horizontalListCont">
                        <div class="listingDate">الشبيبة والطلائع</div>
                    </div></td>
					<td><div><a href="./subpages/shop.html" title="عنوان يكتب هنا"><img src="./Common/images/retail.png" alt="محلات تجارية" title="محلات تجارية"/></a></div>
                    <div class="summaryNewsContainer horizontalListCont">
                        <div class="listingDate">محلات تجارية</div>
                    </div></td>
				  </tr>
				</table>
				
				</div>
            </div>
        </div>
        <div class="Col4SectLast">
        	<div id="BulletListingOption3" class="containersSpace">
                <div class="titlesContainerOpt3">
                    <h2>الاعلانات</h2>
                </div>
                <div><ul class="bulletListingOpt3">
				
				<?php/*
					$dbhost = 'localhost';
					$dbuser = 'id6982079_mechmechdb';
					$dbpass = 'mechmech123';
					$dbname = 'id6982079_mechmechdb';
					$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
					mysqli_set_charset($conn, 'utf8');
					if(!$conn)
						die('could not connect: ' + mysqli_error());
					
					$sql = 'select msg, created from announcements order by created desc';
					$result = mysqli_query($conn, $sql);
					$count = 0;
					if(mysqli_num_rows($result) > 0) {
						while($row = mysqli_fetch_assoc($result)) {
							if($count < 6)
							echo '
							<li><a href="javascript:;"><strong class="dateChangeColor">' .$row["created"].
							'</strong>&nbsp;&nbsp;' .$row["msg"]. '</a></li>';	
							$count++;
						}
						
					} else {
						echo '0 Announcements';
					}
					mysqli_close($conn);*/
				?>
				<a href="./Announcements/announcements.php" style="float:Left;color:black;">لمزيد</a>
                </ul></div>
            </div><br>
            <div id="listingOption3">
            	<div class="Col4SectLast">
                    <div id="listingOneItemBigOpt2">
                        <div class="titlesContainer">
                            <div><h2><a href="./subpages/gallery.html">
                        ألبوم الصور</a></h2></div>
                        </div>
                        <div><a href="./subpages/gallery.html" title="ألبوم الصور"><img src="./Common/images/300-225.gif" alt="ألبوم الصور" title="ألبوم الصور"/></a></div>
                        <a href="./subpages/gallery.html" style="float:Left;color:black;">لمزيد</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearBoth"></div>
        <div class="Col12Sect footerSpecialSpace" style="width:100%" id="footer">
        	<div class="footerContainer">
            <div class="footerLinksCont">
                <ul>
                    <li><a href="#" class="footerFirstLevel">الصفحة الرئيسية</a></li>
                    <li><a href="javascript:;" class="footerFirstLevel">المجلس البلدي</a></li>
                    <li><a href="./subpages/committee.html" class="footerSecondLevel">الاعضاء</a></li>
                    <li><a href="./subpages/presidentWord.html" class="footerSecondLevel">كلمة الرئيس</a></li>
                </ul>
            </div>
            <div class="footerLinksCont">
                <ul>
                    <li><a href="./subpages/history.html" class="footerFirstLevel">نبذة عنا</a></li>
                    <li><a href="./subpages/achievements.html" class="footerFirstLevel">الانجازات</a></li>
                    <li><a href="./subpages/papersAndRequests.html" class="footerFirstLevel">طلبات ومعاملات</a></li>
                </ul>
            </div>
            <div class="footerLinksCont">
                <ul>
                    <li><a href="javascript:;" class="footerFirstLevel">مؤسسات وجمعيات</a></li>
                    <li><a href="./subpages/school.html" class="footerSecondLevel">المدرسة الرسمية</a></li>
                    <li><a href="./subpages/church.html" class="footerSecondLevel">الكنائس</a></li>
                    <li><a href="./subpages/clinic.html" class="footerSecondLevel">المستوصف الخيري</a></li>
                    <li><a href="./subpages/chabibe.html" class="footerSecondLevel">الشبيبة والطلائع</a></li>
                    <li><a href="./subpages/club.html" class="footerSecondLevel">النادي الرياضي</a></li>
                </ul>
            </div>
            <div class="footerLinksCont">
                <ul>
					<li><a href="./subpages/request.html" class="footerFirstLevel">طلب أو شكوى</a></li>
                    <li><a href="./subpages/contact.html" class="footerFirstLevel">اتصل بنا</a></li>
                    <li><a href="./subpages/sitemap.html" class="footerFirstLevel">خريطة الموقع</a></li>
                </ul>
            </div>
            <div class="footerLinksCont latestUpdateCont">
                <div class="margbottom5">اتبعنا على المواقع التالية</div>
                <a href="https://www.facebook.com/groups/2549753991/" target="blank" title="Facebook"><img src="./Common/images/facebook-ico.gif" alt="Facebook" title="Facebook"/></a>
                <a href="javascript:;" title="Android"><img src="./Common/images/android-ico.gif" alt="Android" title="Android"/></a>
                <a href="javascript:;" title="iOS"><img src="./Common/images/ios-ico.gif" alt="iOS" title="iOS"/></a>
            </div>
            <div class="clearBoth"></div>
        </div>
        </div>
        <div class="clearBoth"></div>-->
        <div class="Col12Sect" id="Copyrights" style="width:100%; height:20px">
        	<div style="width:100%;" class="copyrightContainer">جميع الحقوق محفوظة &copy;&emsp;&emsp;<a href="./subpages/contact.html">إتصل بنا</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="./subpages/sitemap.html">خريطة الموقع</a></div>
        </div>
    </div>
</div>
<!-- End Center Container -->

<script>
var slideIndex = 0;
showSlides();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";  
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}    
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " active";
    setTimeout(showSlides, 2500); // Change image every 2.5 sec
}
</script>

</body>
</html>