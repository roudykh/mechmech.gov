/**************************Slider*****************************************/
$(document).ready(function() {
	//Show the paging and activate its first link
	$(".paging").show();
	$(".paging li a:first").addClass("active");
	
	//Get size of the window container, how many windows there are, then determin the size of the window reel.
	var windowWidth = $(".windowContainer").width();
	var windowSum = $(".windowReel li img").size();
	var windowReelWidth = windowWidth * windowSum;
	
	//Adjust the image reel to its new size
	$(".windowReel").css({'width' : windowReelWidth});
	//Paging  and Slider Function
	rotate = function(){
		var triggerID = $active.attr("rel") - 1; //Get number of times to slide
		var window_reelPosition = triggerID * windowWidth; //Determines the distance the image reel needs to slide
	
		$(".paging a").removeClass('active'); //Remove all active class
		$active.addClass('active'); //Add active class (the $active is declared in the rotateSwitch function)
	
		//Slider Animation
		$(".windowReel").animate({left: -window_reelPosition}, 500 );
	}; 
	
	//Rotation  and Timing Event
	rotateSwitch = function(){
		play = setInterval(function(){ //Set timer - this will repeat itself every 7 seconds
			$active = $('.paging a.active').next(); //Move to the next paging
			if ( $active.length === 0) { //If paging reaches the end...
				$active = $('.paging a:first'); //go back to first
			}
			rotate(); //Trigger the paging and slider function
		}, 700000000); //Timer speed in milliseconds (7 seconds)
	};
	
	rotateSwitch(); //Run function on launch
	
	//On Hover
	$(".windowReel li img").hover(function() {
		clearInterval(play); //Stop the rotation
	}, function() {
		rotateSwitch(); //Resume rotation timer
	});	
	
	//On Click
	//var findParent = $(".paging li a").parents(".newsContainer").find();
	
	//alert(findParent)

	$(".paging li a").click(function() {
		$active = $(this); //Activate the clicked paging
		//Reset Timer
		clearInterval(play); //Stop the rotation
		rotate(); //Trigger rotation immediately
		rotateSwitch(); // Resume rotation timer
		return false; //Prevent browser jump to link anchor
	});
});
/****************************End Slider**********************************************/

function OpenWindow(url, w, h) {
    var wnd = window.open(url, null);
    wnd.setSize(w, h);
    wnd.center();
    return false;
}

function OpenWindowddl(url, w, h) {
    var wnd = window.open(url, null);
    return false;
}
function changesite(ddl) {    
    OpenWindowddl($(ddl).val())
}