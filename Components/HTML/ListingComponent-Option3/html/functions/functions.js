/************************************ Tabs *************************************/
$(document).ready(function() {
	initTabs()
});
function initTabs()
{
	$(".navFader li a").click(function() {
		var $container = $(this).parents(".tabsContainer");
		$container.find(".navFader .on").removeClass("on");
		$(this).addClass("on");
		var index =  $container.find(".navFader li a").index($(this));
		$container.find(".subtab").hide();
		$container.find(".subtab:eq("+index+")").fadeIn(200);
	});
	$(".navFader").each(function(){ $(this).find("li a:first").click() });
}
/************************************ End Tabs *************************************/