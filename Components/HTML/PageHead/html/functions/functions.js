var max_font_size = 16;
var min_font_size = 12;

$(function(){
	$('.controls a').click(function(){
		var ourText = $('#contentToEnalrge, #contentToEnalrge td, #contentToEnalrge div').not("");
		var currFontSize = ourText.css('fontSize');
		var finalNum = parseFloat(currFontSize, 12);
		var stringEnding = currFontSize.slice(-2);
		if(this.id == 'large') {
			if(finalNum < max_font_size) finalNum += 2;
		}
		else if (this.id == 'small'){
			if(finalNum > min_font_size) finalNum -=2;
		}
		ourText.css('fontSize', finalNum + stringEnding);
	});
});