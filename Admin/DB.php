<?php

require('./Helper.php');

class DB
{
    protected $pdo;

    function __construct()
    {
        $serverName = env("MYSQL_PORT_3306_TCP_ADDR", "localhost");
        $databaseName = env("MYSQL_INSTANCE_NAME", "id6982079_mechmechdb");
        $username = env("MYSQL_USERNAME", "id6982079_mechmechdb");
        $password = env("MYSQL_PASSWORD", "mechmech123");

        try {
            $this->pdo = new PDO("mysql:host=$serverName;dbname=$databaseName;charset=utf8", $username, $password);

            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            echo "error: " . $e->getMessage();
            die();
        }
    }

    public function all()
    {
        return $this->pdo->query('SELECT * from credentials')
            ->fetchAll();
    }

    public function remove($id)
    {
        return $this->pdo->exec("DELETE from announcements WHERE id = $id ");
    }

    public function add($msg, $date)
    {
		$sql = "INSERT INTO announcements ( msg , created ) VALUES ('$msg','$date')";
		return $this->pdo->exec($sql);
    }
	
	public function addRequest($name, $nbr, $msg, $date)
    {
		$sql = "INSERT INTO requests ( name, tel, msg , created ) VALUES ('$name', '$nbr', '$msg','$date')";
		return $this->pdo->exec($sql);
    }
}
