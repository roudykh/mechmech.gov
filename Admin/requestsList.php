<?php
	session_start();
    include './session.php';
	if(!isset($_SESSION['username'])) {
		header("location: login.php");
		exit();
	}
	
	if(time() - $_SESSION['timeout'] > 900) { //15min
		unset($_SESSION['username'], $_SESSION['password'], $_SESSION['timeout']);
		$_SESSION['valid'] = false;
		header("Location: login.php");
		exit;
	} else {
		$_SESSION['timeout'] = time(); //set new timestamp
	}
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mechmech - Admin</title>

    <link href="v/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="v/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="css/agency.min.css" rel="stylesheet">
    <link href="css/scrollbar.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
	
	<style>
		textarea {
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			width: 100%;
		}		
	</style>
	
</head>

<body id="page-top" class="index">
	
	<!-- Navigation -->
	<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top" style="background-color: #222;">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header page-scroll">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand page-scroll" style="color:white" href="#page-top">Mechmech - Admin</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="hidden">
						<a href="#page-top"></a>
					</li>
					<li>
						<a class="page-scroll" href="./dashboard.php">اعلانات</a>
					</li>
					<li>
						<a class="page-scroll" href="#me">الطلبات</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	
    <section id="me">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">الطلبات</h2>
                </div>
            </div>
            <div>
				<table class="table">
					<thead>
						<tr>
							<th scope="col">Date</th>
							<th scope="col">Name</th>
							<th scope="col">Tel</th>
							<th scope="col">Message</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$dbhost = 'localhost:3306';
							$dbuser = 'root';
							$dbpass = 'mechmech123';
							$dbname = 'mechmechdb';
							$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
							mysqli_set_charset($conn, 'utf8');
							if(!$conn)
								die('could not connect: ' + mysqli_error());
							
							$sql = 'select msg, created, tel, name from requests';
							$result = mysqli_query($conn, $sql);
							if(mysqli_num_rows($result) > 0) {
								while($row = mysqli_fetch_assoc($result)) {
									echo 
									'<tr>
										<td > ' . $row["created"] . ' </td>
										<td > ' . $row["name"] . ' </td>
										<td > ' . $row["tel"] . ' </td>
										<td>
											<textarea  rows="5">
												' . $row["msg"] . '
											</textarea>
										</td>
									</tr>';
								}
							} else {
								echo '<div class="input-group">
										0 requests
									</div>';
							}
							mysqli_close($conn);
						?>
					</tbody>
				</table>
				<a class="btn btn-warning" href = "logout.php" tite = "Logout">Logout</a>
            </div>
        </div>
    </section>

    <script src="v/jquery/jquery.min.js"></script>
    <script src="v/bootstrap/js/bootstrap.min.js"></script>
    <script src="v/jquery/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/agency.min.js"></script>
	
    <script src="js/app.js"></script>

</body>
</html>