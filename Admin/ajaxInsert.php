<?php 

require("DBUtils.php");

header('Content-Type: application/json');

$utils = new DBUtils();

$utils->setUp();

$aResult = array();
if( !isset($_POST['functionname']) ) {
	$aResult['error'] = 'No fct name';
}
if( !isset($_POST['arguments']) ) {
	$aResult['error'] = 'No args';
}

if( !isset($_POST['error']) ) {
	$utils->insert($_POST['arguments']);	
}

echo json_encode($aResult);	

?>