<?php

require_once("DBUtils.php");

$utils = new DBUtils();

$utils->setUp();

$msg = '';

if (isset($_POST['login']) && !empty($_POST['username']) 
   && !empty($_POST['password'])) {
	
   if ($utils->exist($_POST['username'], $_POST['password'])) {
	  $_SESSION['valid'] = true;
	  $_SESSION['timeout'] = time();
	  $_SESSION['username'] = 'admin';
	  $_SESSION['accessLevel'] = 'admin';
   } else {
	  echo 'Wrong username or password';
	  header('location: ./logout.php');
   }
}
?>