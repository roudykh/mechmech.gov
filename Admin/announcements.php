<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mechmech - Announcements</title>

    <link href="v/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="v/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="css/agency.min.css" rel="stylesheet">
    <link href="css/scrollbar.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar-fixed-top" style="background-color:#222">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand page-scroll" href="#page-top">Municipality of Mechmech</a>
            </div>
        </div>
    </nav>
	
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">اعلانات</h2>
                </div>
            </div>
            <div>
				<ul class="list-group">
					<?php
						$dbhost = 'localhost';
						$dbuser = 'id6982079_mechmechdb';
						$dbpass = 'mechmech123';
						$dbname = 'id6982079_mechmechdb';
						$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
						/*mysqli_set_charset($conn, 'utf8');*/
						if(!$conn)
							die('could not connect: ' + mysqli_error());
						
						$sql = 'select msg, created from announcements';
						$result = mysqli_query($conn, $sql);
						if(mysqli_num_rows($result) > 0) {
							while($row = mysqli_fetch_assoc($result)) {
								echo 
								'<li class="list-group-item d-flex justify-content-between align-items-center">
									'.$row["msg"].'
									<span class="badge badge-primary badge-pill">'.$row["created"].'</span>
								</li>';
							}
						} else {
							echo '<li class="list-group-item d-flex justify-content-between align-items-center">
									0 Announcements
								</li>';
						}
						mysqli_close($conn);
					?>
				</ul>
            </div>
        </div>
    </section>

    <script src="v/jquery/jquery.min.js"></script>
    <script src="v/bootstrap/js/bootstrap.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/agency.min.js"></script>
	
    <script src="js/app.js"></script>

</body>
</html>