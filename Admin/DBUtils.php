<?php
require_once("DB.php");	

class DBUtils
{
    public $db;

    function setUp()
    {
        $this->db = new DB();
    }

    function tearDown() {
        unset($this->db);
    }

    function exist($username, $pass) {
        $credentials = $this->db->all();
        foreach ($credentials as $index => $credential) {
            if ($credential['username'] == $username && $credential['pass'] == md5($pass)) {
                return true;
            }
        }
        return false;
    }
	
	function insert($msg) {
		date_default_timezone_set('Asia/Beirut');
		$date = date('Y-m-d', time());
		$this->db->add($msg, $date);
	}
	
	function delete($id) {
		$this->db->remove($id);
	} 

	function insertRequest($name, $nbr, $msg) {
		date_default_timezone_set('Asia/Beirut');
		$date = date('Y-m-d', time());
		$this->db->addRequest($name, $nbr, $msg, $date);
	}	
}
?>
