$(() => {
	
	$('#addRow').click(function() {
		$('#announcements-group').prepend(`
			<div class="input-group">
				<textarea class="form-control custom-control" rows="3" style="resize:none" id="rowData"></textarea>
				<span class="input-group-addon btn btn-danger">X</span>
			</div><br>
		`);
		
		$('#addRow').attr('disabled', true);
	});

	$('#saveRow').click(function() {
		
		var args = $('#rowData').val();
		
		var result;
		
		jQuery.ajax({
			type: "POST",
			url: 'ajaxInsert.php',
			dataType: 'json',
			data: {functionname: 'insert', arguments: args},
			success: function(obj, textstatus) {
				if(!('error' in obj)) {
					result = obj.result;		
				} else {
					console.log('error inserting: ' + obj.error);
				}
			}
		});	
		setTimeout( function() {
			location.reload();
		}, 500);
	});

	
	$('.deleteRow').click(function() {
		
		var args = this.id;
		var result;
		jQuery.ajax({
			type: "POST",
			url: 'ajaxDelete.php',
			dataType: 'json',
			data: {arguments: args},
			success: function(obj, textstatus) {
				if(!('error' in obj)) {
					result = obj.result;	
				} else {
					console.log('error deleting: ' + obj.error);
				}
			}
		});
		setTimeout( function() {
			location.reload();
		}, 500);
	});
	
	
	$('#submitRequest').click(function() {
		var name = $('#name').val();
		var nbr = $('#nbr').val();
		var msg = $('#request').val();
		
		var result;
		
		jQuery.ajax({
			type: "POST",
			url: 'ajaxRequest.php',
			dataType: 'json',
			data: {functionname: 'insert', name: name, nbr: nbr, msg: msg},
			success: function(obj, textstatus) {
				if(!('error' in obj)) {
					result = obj.result;		
				} else {
					console.log('error inserting request: ' + obj.error);
				}
			}
		});	
		setTimeout( function() {
			location.reload();
		}, 500);
	});
	
});	