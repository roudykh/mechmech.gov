<?php 

require("DBUtils.php");

header('Content-Type: application/json');

$utils = new DBUtils();

$utils->setUp();

$aResult = array();
if( !isset($_POST['arguments']) ) {
	$aResult['error'] = 'No args';
}

if( !isset($_POST['error']) ) {
	$utils->delete($_POST['arguments']);	
}

echo json_encode($aResult);	
?>