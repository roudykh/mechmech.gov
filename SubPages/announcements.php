<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="description" content=""/> 
	<meta name="keywords" content=""/>
    <link rel="stylesheet" type="text/css" href="../Common/styles/content.css"/>
    <link rel="stylesheet" type="text/css" href="../Common/styles/common.css"/>
    <link rel="stylesheet" type="text/css" href="../Common/themes/grey-burgundy/grey-burgundy.css"/>
    <link rel="stylesheet" type="text/css" href="../Common/styles/layout.css"/>
    <script type="text/javascript" src="../Common/functions/jquery-1.4.min.js"></script>
    <!-- Header Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/Header/html/styles/header.css"/>
    <!-- Menu Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/Menu/html/styles/menu.css"/>
    <script type="text/javascript" src="../Components/HTML/Menu/html/functions/functions.js"></script>
    <!-- Newsticker Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/NewsTicker/html/styles/newsticker.css"/>
    <!-- Breadcrumb Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/Breadcrumb/html/styles/breadcrumb.css"/>
    <!-- Page Head Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/PageHead/html/styles/pagehead.css"/>
    <script type="text/javascript" src="../Components/HTML/PageHead/html/functions/functions.js"></script>
    <!-- Footer Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/Footer/html/styles/footer.css"/>
    <!-- Copyrights Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/Copyright/html/styles/copyright.css"/>
    <!-- Listing Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/ListingComponent/html/styles/listing-component.css"/>
    <!-- Bullets Listing Option 3 Styles -->
    <link rel="stylesheet" type="text/css" href="../Components/HTML/BulletListing-Option3/html/styles/bullet-listing-opt3.css"/>
	<link rel="shortcut icon" href="../Common/images/favicon.ico"/>
	<title>الاعلانات</title>
</head>
<body>
<!-- End Top Header -->
<!-- Header Container -->
<div class="headerCont" id="header" style="width:100%">
    <div class="logosHeader">
    	<div class="mainLogoContainer"><img src="../Common/images/minstry-logo.gif" /></div>
        <div class="clearBoth"></div>
  	</div>
</div>
<!-- End Header Container -->
<!-- Menu Container -->
<div class="menuCont" id="menu">
	<div class="boxContainer">
	<div class="menuBtnsCont" style="width:100%">
        <ul class="menuBtnsList">
            <li><a href="../index.php" class="firstMenuBtn">الصفحة الرئيسية</a></li>
			<li><a href="javascript:;" onmouseout="MM_showHideLayers('sub1','','hide');document.getElementById('menuBtn1').className='';" 
					onmouseover="MM_showHideLayers('sub1','','show');document.getElementById('menuBtn1').className='highlightMenuBtn';" 
					id="menuBtn1">المجلس البلدي</a><div class="relativePos highZindex"><div class="absoluteSubMenu" 
					id="sub1" onmouseout="MM_showHideLayers('sub1','','hide');document.getElementById('menuBtn1').className='';" 
					onmouseover="MM_showHideLayers('sub1','','show');document.getElementById('menuBtn1').className='highlightMenuBtn';">
					<div class="subMenuContainer">
                <div class="subMenuSection"><ul>
                <li><a href="../subpages/committee.html" class="mainTitle">المجلس</a></li>
                <li><a href="../subpages/presidentWord.html" class="mainTitle">كلمة الرئيس</a></li>
                </ul></div>
                <div class="clearBoth"></div>
            </div></div></div></li>
            <li><a href="../subpages/history.html">نبذة عنا</a></li>
            <li><a href="../subpages/achievements.html">الانجازات</a></li>
            <li><a href="../subpages/committee.html">طلبات ومعاملات</a></li>
            <li><a href="javascript:;" onmouseout="MM_showHideLayers('sub3','','hide');document.getElementById('menuBtn3').className='';" 
					onmouseover="MM_showHideLayers('sub3','','show');document.getElementById('menuBtn3').className='highlightMenuBtn';" 
					id="menuBtn3">مؤسسات وجمعيات</a><div class="relativePos highZindex"><div class="absoluteSubMenu" 
					id="sub3" onmouseout="MM_showHideLayers('sub3','','hide');document.getElementById('menuBtn3').className='';" 
					onmouseover="MM_showHideLayers('sub3','','show');document.getElementById('menuBtn3').className='highlightMenuBtn';">
					<div class="subMenuContainer">
                <div class="subMenuSection"><ul>
                <li><a href="../subpages/school.html" class="mainTitle">المدرسة</a></li>
                <li><a href="../subpages/clinic.html" class="mainTitle">المستوصف الخيري</a></li>
                <li><a href="../subpages/church.html" class="mainTitle">الكنائس</a></li>
                </ul></div>
                <div class="subMenuSectionLast"><ul>
                <li><a href="../subpages/chabibe.html" class="mainTitle">الشبيبة والطلائع</a></li>
                <li><a href="../subpages/club.html" class="mainTitle">النادي الرياضي</a></li>
                <li><a href="../subpages/shop.html" class="mainTitle">محلات تجارية</a></li>
                </ul></div>
                <div class="clearBoth"></div>
            </div></div></div></li>
            <li><a href="../subpages/gallery.html">الصور</a></li>
            <li><a href="../Announcements/announcements.php" class="selected">الاعلانات</a></li>
            <li><a href="../subpages/contact.html">اتصل بنا</a></li>
        </ul>
        </div>
    </div>
</div>
<!-- End Menu Container -->
<!-- Center Container -->
<div class="">
	<div class="centerContBackground">
        <div class="Col9SectLast containersNoSpace" style="width:100%">
        <div class="containersSpace">
        	<div class="boxContainer">
                <div class="subpageHead">
                    <h1 class="mainTitleFloat">الاعلانات</h1>
                    <div class="clearBoth"></div>
                </div>
            </div>
        </div>
        	<div class="Col12Sect">
            <!-- Page Content Start Here -->
            <div class="boxContainer">
            	
				<?php
					$dbhost = 'localhost';
					$dbuser = 'id6982079_mechmechdb';
					$dbpass = 'mechmech123';
					$dbname = 'id6982079_mechmechdb';
					$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
					mysqli_set_charset($conn, 'utf8');
					if(!$conn)
						die('could not connect: ' + mysqli_error());
					
					$sql = 'select msg, created from announcements order by created desc';
					$result = mysqli_query($conn, $sql);
					$count = 0;
					if(mysqli_num_rows($result) > 0) {
						while($row = mysqli_fetch_assoc($result)) {
							echo '
							<li><p><strong class="dateChangeColor">' .$row["created"].
							'</strong>&emsp;' .$row["msg"]. '</p></li><br>';	
							$count++;
						}
						
					} else {
						echo '0 Announcements';
					}
					mysqli_close($conn);
				?>
				
            </div>
            <!-- Page Content End Here -->
            </div>
            <div class="clearBoth"></div>
        </div>
        <div class="clearBoth"></div>
        <div class="Col12Sect footerSpecialSpace" id="footer" style="width:100%">
        	<div class="footerContainer">
            <div class="footerLinksCont">
                <ul>
                    <li><a href="../index.php" class="footerFirstLevel">الصفحة الرئيسية</a></li>
                    <li><a href="javascript:;" class="footerFirstLevel">المجلس البلدي</a></li>
                    <li><a href="../subpages/committee.html" class="footerSecondLevel">الاعضاء</a></li>
                    <li><a href="../subpages/PresidentWord.html" class="footerSecondLevel">كلمة الرئيس</a></li>
                </ul>
            </div>
            <div class="footerLinksCont">
                <ul>
                    <li><a href="../subpages/history.html" class="footerFirstLevel">نبذة عنا</a></li>
                    <li><a href="../subpages/achievements.html" class="footerFirstLevel">الانجازات</a></li>
                    <li><a href="../subpages/papersAndRequests.html" class="footerFirstLevel">طلبات ومعاملات</a></li>
                </ul>
            </div>
            <div class="footerLinksCont">
                <ul>
                    <li><a href="javascript:;" class="footerFirstLevel">مؤسسات وجمعيات</a></li>
                    <li><a href="../subpages/school.html" class="footerSecondLevel">المدرسة الرسمية</a></li>
                    <li><a href="../subpages/church.html" class="footerSecondLevel">الكنائس</a></li>
                    <li><a href="../subpages/clinic.html" class="footerSecondLevel">المستوصف الخيري</a></li>
                    <li><a href="../subpages/chabibe.html" class="footerSecondLevel">الشبيبة والطلائع</a></li>
                    <li><a href="../subpages/club.html" class="footerSecondLevel">النادي الرياضي</a></li>
                </ul>
            </div>
            <div class="footerLinksCont">
                <ul>
                    <li><a href="../subpages/contact.html" class="footerFirstLevel">اتصل بنا</a></li>
                    <li><a href="../subpages/sitemap.html" class="footerFirstLevel">خريطة الموقع</a></li>
                </ul>
            </div>
            <div class="footerLinksCont">
                &nbsp;
            </div>
            <div class="footerLinksCont">
                &nbsp;
            </div>
            <div class="footerLinksCont latestUpdateCont">
                <div class="margbottom5">اتبعنا على المواقع التالية</div>
                <a href="javascript:;" title="Facebook"><img src="../Common/images/facebook-ico.gif" alt="Facebook" title="Facebook"/></a>
                <a href="javascript:;" title="Android"><img src="../Common/images/android-ico.gif" alt="Android" title="Android"/></a>
                <a href="javascript:;" title="iOS"><img src="../Common/images/ios-ico.gif" alt="iOS" title="iOS"/></a>
            </div>
            <div class="clearBoth"></div>
        </div>
        </div>
        <div class="clearBoth"></div>
        <div class="Col12Sect" id="Copyrights" style="width:100%">
        	<div class="copyrightContainer">جميع الحقوق محفوظة &copy;&emsp;&emsp;<a href="../subpages/contact.html">إتصل بنا</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="../subpages/sitemap.html">خريطة الموقع</a></div>
        </div>
    </div>
</div>
<!-- End Center Container -->
</body>
</html>